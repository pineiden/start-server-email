import os
import sys
import ssl
from smtplib import SMTP
from email.message import EmailMessage
from email.mime.text import MIMEText
from datetime import datetime, timezone
from email.utils import formatdate

if __name__== "__main__":
    from_mail=os.environ.get("EMAIL_FROM")
    password=os.environ.get("EMAIL_PASSWORD")
    host=os.environ.get("EMAIL_HOST")
    port=os.environ.get("EMAIL_PORT")
    print(from_mail, password, host, port)
    servidor=sys.argv[1]
    servicios=sys.argv[2].split()
    print(sys.argv[3])
    destino, email=sys.argv[3].split(",")
    tema=f"Se ha (re)iniciado el servidor! Revisar estado de servicios en {servidor}"
    mensaje=f"""Hola {destino}!\n El servidor {servidor} se ha reiniciado, revisa si los servicios: 
    {servicios} 
    están operando con normalidad

    saludines!
    """
    #print(tema)
    #print(mensaje)
    #print("Enviando emails")
    msg = EmailMessage()
    msg.set_content(mensaje)
    #print(timezone.utc)
    #dt =  datetime.now(tz=timezone.utc)
    #print("DT", dt)
    date_str = formatdate(localtime=True)
    #print("Date str", date_str)
    msg['Date'] = date_str
    msg["Subject"] = tema
    msg["From"] = from_mail
    msg["To"] = email
    context = ssl.create_default_context()
    with SMTP(host, port) as server:
        server.ehlo()
        #print("Iniciando ssl")
        server.starttls(context=context)
        server.ehlo()
        username =  from_mail.split("@")[0]
        #print("Username login", username)
        server.login(username, password)
        #print("PAYLOAD", msg.get_payload())
        #print(f"Login exitoso, sending {msg.as_string()}")
        server.send_message(msg)
